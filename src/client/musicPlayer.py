from machine import Pin, PWM
import time


class MusicPlayer:
    C6 = 1047
    CS6 = 1109
    D6 = 1175
    DS6 = 1245
    E6 = 1319
    F6 = 1397
    FS6 = 1480
    G6 = 1568
    GS6 = 1661
    A6 = 1760
    AS6 = 1865
    B6 = 1976
    C7 = 2093
    CS7 = 2217
    D7 = 2349
    DS7 = 2489
    E7 = 2637
    F7 = 2794
    FS7 = 2960
    G7 = 3136
    GS7 = 3322
    A7 = 3520
    AS7 = 3729
    B7 = 3951

    song = [
        E7,
        E7,
        0,
        E7,
        0,
        C7,
        E7,
        0,
        G7,
        0,
        0,
        0,
        G6,
        0,
        0,
        0,
        C7,
        0,
        0,
        G6,
        0,
        0,
        E6,
        0,
        0,
        A6,
        0,
        B6,
        0,
        AS6,
        A6,
        0,
        G6,
        E7,
        0,
        G7,
        A7,
        0,
        F7,
        G7,
        0,
        E7,
        0,
        C7,
        D7,
        B6,
    ]

    speaker = Pin(14, Pin.OUT)  # D5

    def playMusic(self):
        buz = PWM(self.speaker)
        for note in self.song:
            if note == 0:  # Special case for silence
                buz.duty(0)
            else:
                buz.freq(int(note / 10))
                buz.duty(50)
            time.sleep(0.15)
        buz.duty(0)
        buz.deinit()

    def playBuz(self):
        buz = PWM(self.speaker)
        buz.freq(432)
        buz.duty(50)

        time.sleep(0.10)
        
        buz.duty(0)
        buz.deinit()
