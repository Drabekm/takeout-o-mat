# takeout-o-mat



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Drabekm/takeout-o-mat.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Drabekm/takeout-o-mat/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be instaf�VB��V�ǒ��6�FB&WV�&V�V�G27V'6V7F����РТ22W6vPХW6RW���W2Ɩ&W&�ǒ��B6��rF�RW�V7FVB�WGWB�b��R6���Bw2�V�gV�F��fR��Ɩ�RF�R6���W7BW���R�bW6vRF�B��R6�FV���7G&FR�v���R&�f�F��rƖ�2F���&R6���7F�6FVBW���W2�bF�W�&RF�����rF�&V6��&ǒ��6�VFR��F�R$TD�R�РТ227W�'@ХFV��V��Rv�W&RF�W�6�v�F�f�"�V���B6�&R�6��&��F����b��77VRG&6�W"�6�B&�����V���FG&W72�WF2�РТ22&�F� Ф�b��R�fR�FV2f�"&V�V6W2��F�RgWGW&R��B�2v��B�FVF�Ɨ7BF�V���F�R$TD�R�РТ226��G&�'WF��pХ7FFR�b��R&R�V�F�6��G&�'WF���2�Bv�B��W"&WV�&V�V�G2&Rf�"66WF��rF�V��РФf�"V��Rv��v�BF���R6��vW2F���W"&��V7B��Bw2�V�gV�F��fR6��RF�7V�V�FF�������rF�vWB7F'FVB�W&�2F�W&R�267&�BF�BF�W�6��V�B'V��"6��RV�f�&���V�Bf&�&�W2F�BF�W��VVBF�6WB���RF�W6R7FW2W�Ɩ6�B�F�W6R��7G'V7F���26�V�B�6�&RW6VgV�F���W"gWGW&R6V�b�РХ��R6��6�F�7V�V�B6����G2F�Ɩ�BF�R6�FR�"'V�FW7G2�F�W6R7FW2�V�F�V�7W&R��v�6�FRVƗG��B&VGV6RF�RƖ�VƖ���BF�BF�R6��vW2��GfW'FV�Fǒ'&V�6��WF���r��f��r��7G'V7F���2f�"'V���rFW7G2�2W7V6��ǒ�V�gV��b�B&WV�&W2W�FW&��6WGW�7V6�27F'F��r6V�V�V�6W'fW"f�"FW7F��r��'&�w6W"�РТ22WF��'2�B6���v�VFv�V�@Х6��r��W"&V6�F���F�F��6Rv���fR6��G&�'WFVBF�F�R&��V7B�РТ22Ɩ6V�6PФf�"�V�6�W&6R&��V7G2�6���r�B�2Ɩ6V�6VB�РТ22&��V7B7FGW0Ф�b��R�fR'V��WB�bV�W&w��"F��Rf�"��W"&��V7B�WB��FRBF�RF��bF�R$TD�R6���rF�BFWfV���V�B�26��vVBF�v��"7F�VB6���WFVǒ�6��V��R��6���6RF�f�&���W"&��V7B�"f��V�FVW"F�7FW��2���F��W"�"�v�W"����v��r��W"&��V7BF��VWv���r���R6��6���R�W�Ɩ6�B&WVW7Bf�"���F��W'2��