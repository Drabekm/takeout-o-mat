from machine import Pin, PWM, Timer
import time
import network
import select
import socket


class ButtonType:
    Km = 1
    Hd = 2
    Fox = 3


# IO
button = Pin(5, Pin.IN)  # D1
buttonLED = Pin(2, Pin.OUT)  # D4
ledUpper = Pin(4, Pin.OUT)  # D2
ledBottom = Pin(0, Pin.OUT)  # D3
speaker = Pin(14, Pin.OUT)  # D5

# KONICA BOX

# currentButtonType = ButtonType.Km

# responseLEDs = {
#     ButtonType.Hd: ledBottom, # I got the pins wrong on Konica box lol
#     ButtonType.Fox: ledUpper
# }

# HELPDESK BOX

currentButtonType = ButtonType.Hd

responseLEDs = {ButtonType.Km: ledUpper, ButtonType.Fox: ledBottom}

song = [
    E7,
    E7,
    0,
    E7,
    0,
    C7,
    E7,
    0,
    G7,
    0,
    0,
    0,
    G6,
    0,
    0,
    0,
    C7,
    0,
    0,
    G6,
    0,
    0,
    E6,
    0,
    0,
    A6,
    0,
    B6,
    0,
    AS6,
    A6,
    0,
    G6,
    E7,
    0,
    G7,
    A7,
    0,
    F7,
    G7,
    0,
    E7,
    0,
    C7,
    D7,
    B6,
]

ledUpper.value(1)
ledBottom.value(1)
buttonLED.value(1)

lastButtonValue = 0
debounceTimer = Timer(-1)
ledTurnOffTimer = Timer(-1)

# network
wifi = network.WLAN(network.STA_IF)
wifi.active(True)
wifi.connect("")
# wifi.connect("PUX_GUEST", "puxpuxpux")

# server info
server_ip = "jedovachyse.eu"
server_port = 12345

time.sleep(1)
print("Wifi connected?")
print(wifi.isconnected())

ledBottom.value(0)

server_addr_info = socket.getaddrinfo(server_ip, server_port)
addr = server_addr_info[0][-1]

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.settimeout(None)
serverSocket.connect(addr)

print("Socket connected")

ledUpper.value(0)
ledBottom.value(0)
buttonLED.value(0)


def onLedTurnoffTimerTick(huh):
    global ledUpper
    global ledBottom
    ledUpper.value(0)
    ledBottom.value(0)


def onButtonPress():
    global serverSocket
    serverSocket.send(str(currentButtonType))


def startDebounceTimer():
    debounceTimer.init(period=100, mode=Timer.PERIODIC, callback=checkState)


def checkState(huh):
    global button
    global lastButtonValue

    buttonValue = button.value()
    if buttonValue and buttonValue != lastButtonValue:
        onButtonPress()

    buttonLED.value(buttonValue)
    lastButtonValue = buttonValue


def play(buz_pin, notes, delay, active_duty=50):
    buz = PWM(buz_pin)
    for note in notes:
        if note == 0:  # Special case for silence
            buz.duty(0)
        else:
            buz.freq(int(note / 10))
            buz.duty(active_duty)
        time.sleep(delay)
    buz.duty(0)
    buz.deinit()


startDebounceTimer()

while True:
    inputFromServer = serverSocket.recv(1024)
    if inputFromServer != b"":
        responseIndex = int(inputFromServer)

        if responseIndex != currentButtonType:
            print(responseIndex)
            responseLEDs[responseIndex].value(1)
            play(speaker, song, 0.15)
            ledTurnOffTimer.init(
                period=60000, mode=Timer.ONE_SHOT, callback=onLedTurnoffTimerTick
            )


# IRQ handler
# def buttonInterupt(pin):
# buttonLED.value(button.value())


# if button.value():
#     print("Button interupt")
#     print(serverSocket)
#     serverSocket.send("Hello IRQ")
#     print(f"response {serverSocket.recv(1024)}")

# serverSocket.close()
# serverSocket = socket.socket()
# serverSocket.connect(addr)

# def buttonInteruptDown(pin):
#     buttonLED.value(0)


# attachInterupt(None)


# def checkIfCanRead() -> bool:
#     r, _, _ = select.select([serverSocket], [], [])

# while(True):
#     pass
#     # if (button.value()):
#     #     s = socket.socket()
#     #     s.connect(addr)
#     #     print("first sending")
#     #     s.send("Ahoj")
#     #     print("seccond sending")
#     #     s.send("Ahoj")
#     #     print("receaving")
#     #     print(s.recv(100))
#     #     s.close()
