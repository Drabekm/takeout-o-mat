import network
import time

class Network:

    def __init__(self, ssid: str, password: str):
        self.ssid = ssid
        self.password = password

        self.wifi = network.WLAN(network.STA_IF)
        self.wifi.active(True)
        self.wifi.connect(ssid, password)
    
    def SendButtonPress(self, deviceType):
        print("Start sending button press")
        self.__ensureWifiIsConnected()
        
        pass

    def __ensureWifiIsConnected(self):
        print("Making sure wifi is connected...")
        while (self.wifi.isconnected()):
            self.wifi.connect(self.ssid, self.password)
            time.sleep(1)
            print("Failed to connect, trying again...")
