import socket
import threading

# Function to handle a client connection
def handle_client(client_socket, clients):
    while True:
        try:
            data = client_socket.recv(1024)
            if not data:
                break
            message = data.decode()
            print(f"Received: {message}")
            
            # Broadcast the message to all other clients
            for other_client in clients:
                if other_client != client_socket:
                    other_client.send(data)
        
        except Exception as e:
            print(f"Error: {e}")
            break

    # Remove the disconnected client from the list
    clients.remove(client_socket)
    client_socket.close()

# Create a socket server
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(('0.0.0.0', 12345))
server.listen(5)
print("Server listening on port 12345")

clients = []

while True:
    client_socket, addr = server.accept()
    print(f"Accepted connection from {addr}")
    
    # Limit the number of connections to 5
    if len(clients) >= 15:
        print("Connection limit reached. Rejecting new connection.")
        client_socket.close()
    else:
        clients.append(client_socket)
        
        # Start a new thread to handle the client
        client_thread = threading.Thread(target=handle_client, args=(client_socket, clients))
        client_thread.start()
