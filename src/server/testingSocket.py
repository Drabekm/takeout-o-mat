import socket

def send_message(message, host='localhost', port=5555):
    # Create a socket object
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to the server
        client_socket.connect((host, port))

        # Send the message
        client_socket.sendall(message.encode('utf-8'))

        # Receive the response from the server
        response = client_socket.recv(1024)
        print(f"Received from server: {response.decode('utf-8')}")

    except Exception as e:
        print(f"Error: {e}")

    finally:
        # Close the socket
        client_socket.close()

if __name__ == "__main__":
    # Replace 'localhost' and 5555 with the actual host and port of your server
    server_host = 'jedovachyse.eu'
    server_port = 12345

    # Replace 'Hello, server!' with the message you want to send
    message_to_send = 'Hello, server!'

    send_message(message_to_send, server_host, server_port)