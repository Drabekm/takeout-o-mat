#   |=======================================================================|
#   | Quick and dirt (and partly made by chatGPT) server for takeout-o-mats |
#   | to connect to and share information which takeout-o-mat was activated |
#   |=======================================================================|

import socket
from enum import Enum
import json

class Machine(Enum):
    Nothing = 0
    KM = 1
    Fox = 2
    HD = 3

class Server:
    def __init__(self) -> None:
        self.host = '192.168.1.247'
        self.port = 12345
        self.current_active_machine : Machine = Machine.Nothing

    def __reset_active_machine_state(self):
        self.current_active_machine = Machine.Nothing

    def __set_active_machine_state(self, value: int) -> None:
        # todo: don't set state, if 
        self.current_active_machine = value

    def __return_active_machine_state(self) -> str:
        return self.current_active_machine

    def __handle_message(self, client_socket: socket) -> str:
        data = client_socket.recv(1024).decode('utf-8')
        print(f"Received data: {data}")
        request_dict = json.load(data)

        client_socket.send("Nazdar")
        # command = request_dict["command"]        
        # if command == "set":
        #     print("Sending OK reply")
        #     client_socket.send(1)
        # elif command == "get":
        #     print("Sending current active machine")
        #     client_socket.send(self.current_active_machine)

        pass

    def __recieve_messages(self, server_socket: socket):
        while True:
            client_socket, client_address = server_socket.accept()
            print(f"Connection from {client_address}")

            self.__handle_message(client_socket)

            client_socket.close()

    def start_server(self):
        
        server_socket: socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.bind((self.host, self.port))
        server_socket.listen(5)

        print(f"Server listening on {self.host}:{self.port}")

        self.__recieve_messages(server_socket)

        

server = Server()
server.start_server()
