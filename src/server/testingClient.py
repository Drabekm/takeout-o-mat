import socket
import threading

# Function to receive messages from the server
def receive_messages(client_socket):
    while True:
        try:
            data = client_socket.recv(1024)
            if not data:
                break
            message = data.decode()
            print(f"Received from server: {message}")
        except Exception as e:
            print(f"Error: {e}")
            break

# Create a socket client
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(('jedovachyse.eu', 12345))  # Connect to the server (change the IP and port as needed)

# Start a thread to receive messages from the server
receive_thread = threading.Thread(target=receive_messages, args=(client,))
receive_thread.start()

while True:
    try:
        # Read input from the keyboard
        message = input("Enter a message to send to the server: ")
        
        # Send the message to the server
        client.send(message.encode())
    except KeyboardInterrupt:
        print("Client terminated.")
        break
    except Exception as e:
        print(f"Error: {e}")

# Close the client socket
client.close()